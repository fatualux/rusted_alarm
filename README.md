[![License](https://img.shields.io/badge/License-GPL%20v3-blue.svg)](http://www.gnu.org/licenses/gpl-3.0)   

**RUSTED ALARM**

A small timer written in Rust.

I am not a developer, and I wrote this small application with the help of Codeium (https://codeium.com/), with the extension for Vim (for text completion) and Code OSS (for the live chat, if getting errors during compilation).

it basiclly prompts the user for interval an audio volume, then sets a timer and a progress bar according to the user choices.

**DEPENDENCIES**

- [**Rust**](https://www.rust-lang.org/)
- *alsa and alsa-plugins* for audio playback

**INSTALLATION**

Simply clone this repository, then:

'cd rusted_alarm'
'cargo build'
'cargo run'
